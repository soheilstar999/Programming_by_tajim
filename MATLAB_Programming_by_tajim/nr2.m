%Solving an equation by Newton Raphson Method with MATLAb coding
clear all;
close all;
clc;
f=@(x) cos(x) - 3*x+1 % the function to be solved
df=@(x) - sin(x) -3 %the darivative of that function
a=0;b=1; x=a;
for i=1:1:100
    x1 = x - (f(x)/df(x));
    x = x1;
end
solve=x;
fprintf('Approximate root is %.15f',solve);
a=0;b=1; x=a;
error(5)=0;
for i=1:1:5
    x1 = x - (f(x)/df(x));
    x = x1;
    error(i)=x1-solve;
end
plot(error);
xlabel('Number of iteration');
ylabel('Error');
title('Error vs number of iteration');
