%Question: Evaluate the integral 1/(1+x) within limits 0 to 6
clc;
clear all;
close all;
f=@(x)1/(1+x); %Change here for different function
a=0;b=6; %Given limits
n=b-a; %Number of intervals
h=(b-a)/n;
p=0;
for i=a:b
p=p+1;
x(p)=i;
y(p)=1/(1+i); %Change here for different function
end
l=length(x);
x
y
answer=(h/2)*((y(1)+y(l))+2*(sum(y)-y(1)-y(l)))