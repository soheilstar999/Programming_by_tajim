%Question: Evaluate the integral 1/(1+x) within limits 0 to 6
clc;
clear all;
close all;
%f=@(x)1/(1+x^2); %Change here for different function
f1=input('enter the equation:','s');
f=inline(f1);
a=0;b=6; %Given limits x0 to x6
n=b-a; %Number of intervals
h=(b-a)/n;
p=0;
for i=a:b
p=p+1;
x(p)=i;
y(p)=1/(1+i); %Change here for different function
end
l=length(x);
x
y
answer=(h/3)*((y(1)+y(l))+2*(y(3)+y(5))+4*(y(2)+y(4)+y(6)))