/*
 * day17_watch.c
 *
 * Created: 11/7/2017 8:30:58 PM
 * Author : User
 */ 

#define F_CPU 8000000UL
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include "lcd.h"
#include "adc.h"
#include <avr/interrupt.h>

char line[16];
int count =0;

unsigned int h = 12;
unsigned int h24 = 0;
unsigned int m = 0;
unsigned int s = 0;
unsigned int sc = 0;

float adc_value = 0;
float adc_volt = 0;
float mili_volt = 0;
float temp = 0;


void Timer_0_Init(void)
{
	// TCCR1 for 16 bit
	TCCR1B &= ~(1<<CS10); // prescalling 256 - 001
	TCCR1B &= ~(1<<CS11);
	TCCR1B |= (1<<CS12);
	
	// sob gula 0 korle wave form generation normal mode
	TCCR1B &= ~(1<<WGM10); //timer normal mode
	TCCR1B &= ~(1<<WGM11); //timer normal mode
	TCCR1B &= ~(1<<WGM12); //timer normal mode
	TCCR1B &= ~(1<<WGM13); //timer normal mode
	
	TIMSK |= (1<<OCIE1A); //output compare match enable
	TCNT1 = 0; // clear time counter
	OCR1A = 31250;
	sei(); // set enable interrupt
}

ISR(TIMER1_COMPA_vect)
{
	s++;
	
	if(s>=60)
	{
		s=0;
		m++;
	}
	else if(m>=60)
	{
		m=0;
		h++;
		h24++;
	}
	else if(h>12)
	{
		h=1;
	}
	else if(h24>=24)
	{
		h24=0;
	}
	TCNT1 = 0; // clear time counter
	
}


int main(void)
{
	LCDInit();
	adc_init();
	//char degree='�';
	//char degree = 176;
	Timer_0_Init(); // initilizing timer = 0
	//int hc=0,mc=0,sc=0;
	while (1) 
    {
		sprintf(line, "TIME %.2u:%.2u:%.2u", h, m, s);
		LCDGotoXY(1,1);
		LCDString(line);
		
		if (h24>=0 && h24<=11)
		{
			sprintf(line, "AM");
		}
		else
		{
			sprintf(line, "PM");
		}
		LCDGotoXY(15,1);
		LCDString(line);
		_delay_ms(500);
		
		adc_value = adc_read(0);
		adc_volt = ((adc_value * 2.56)/1024.00)*2;
		mili_volt = adc_volt*1000;
		temp = mili_volt/10;
		
		sprintf(line, "Temperature %.0f`C",temp);
		LCDGotoXY(1,2);
		LCDString(line);
		//_delay_ms(500);
		
		if(bit_is_clear(PIND,1))
		{
			_delay_us(20); //debounce delay
			sc++;
			
			while(sc==1)
			{
				sprintf(line, "You Select SET");
				LCDGotoXY(1,1);
				LCDString(line);
				
				sprintf(line, "SET hour: ");
				LCDGotoXY(1,2);
				LCDString(line);
				
				sprintf(line, "%.2u",h);
				LCDGotoXY(11,2);
				LCDString(line);
				
				if(bit_is_clear(PIND,2))
				{
					while(bit_is_clear(PIND,2))
					_delay_us(10);
					h++;
					h24++;
				}
				
				else if(bit_is_clear(PIND,1))
				{
					_delay_us(10); //debounce delay
					sc++;
					break;
				}
			}
			while(sc==2)
			{
				sprintf(line, "You Select SET");
				LCDGotoXY(1,1);
				LCDString(line);
				
				sprintf(line, "SET Minute: ");
				LCDGotoXY(1,2);
				LCDString(line);
				
				sprintf(line, "%.2u",m);
				LCDGotoXY(12,2);
				LCDString(line);
				
				if(bit_is_clear(PIND,2))
				{
					while(bit_is_clear(PIND,2))
					_delay_us(10);
					m++;
				}
				
				else if(bit_is_clear(PIND,1))
				{
					_delay_us(10); //debounce delay
					sc++;
					break;
				}
			}
			if (sc>2)
			{
				sc=0;
			}
		}	
	}		  
	return 0;
}

// on -- bit_is_clear(PIND,1) -- bit_is_clear(PIND,2)
// off -- bit_is_set(PIND,1) -- bit_is_set(PIND,2)
// set = pin 1 , change = pin 2